/**********************************************************************
 * file:  sr_router.c
 * date:  Mon Feb 18 12:50:42 PST 2002
 * Contact: casado@stanford.edu
 *
 * Description:
 *
 * This file contains all the functions that interact directly
 * with the routing table, as well as the main entry method
 * for routing.
 *
 **********************************************************************/

#include <stdio.h>
#include <assert.h>


#include "sr_if.h"
#include "sr_rt.h"
#include "sr_router.h"
#include "sr_protocol.h"
#include "sr_arpcache.h"
#include "sr_utils.h"

/*---------------------------------------------------------------------
 * Method: sr_init(void)
 * Scope:  Global
 *
 * Initialize the routing subsystem
 *
 *---------------------------------------------------------------------*/
void sr_init(struct sr_instance* sr)
{
  /* REQUIRES */
  assert(sr);

  /* Initialize cache and cache cleanup thread */
  sr_arpcache_init(&(sr->cache));

  pthread_attr_init(&(sr->attr));
  pthread_attr_setdetachstate(&(sr->attr), PTHREAD_CREATE_JOINABLE);
  pthread_attr_setscope(&(sr->attr), PTHREAD_SCOPE_SYSTEM);
  pthread_attr_setscope(&(sr->attr), PTHREAD_SCOPE_SYSTEM);
  pthread_t thread;

  pthread_create(&thread, &(sr->attr), sr_arpcache_timeout, sr);
} /* -- sr_init -- */



/*---------------------------------------------------------------------
  Check source ip with ip_blacklist
  Goal 1 : check whether the source ip is black ip or not
  Goal 2 : Print Log
  - Format  :  " [Source ip blocked] : <source ip> "
  e.g.) [Source ip blocked] : 10.0.2.100
*/
int ip_black_list(struct sr_ip_hdr* iph)
{
  int blk = 0;
  char ip_blacklist[20]="10.0.2.0";
  char mask[20]="255.255.255.0";

  /**************** fill in code here *****************/
  uint32_t src = iph->ip_src; /* source ip of client2 */
  /* bitwise calc: AND*/
  if (inet_addr(ip_blacklist) == (src & inet_addr(mask))){
    blk = 1; /* should be blocked */
    printf(" [Source ip blocked] : ");
    fflush(stdout);
    print_addr_ip_int(ntohl(src));
  }
  /****************************************************/
  return blk;
}
/*---------------------------------------------------------------------
 * Method: sr_handlepacket(uint8_t* p,char* interface)
 * Scope:  Global
 *
 * This method is called each time the router receives a packet on the
 * interface.  The packet buffer, the packet length and the receiving
 * interface are passed in as parameters. The packet is complete with
 * ethernet headers.
 *
 * Note: Both the packet buffer and the character's memory are handled
 * by sr_vns_comm.c that means do NOT delete either.  Make a copy of the
 * packet instead if you intend to keep it around beyond the scope of
 * the method call.
 *
 *---------------------------------------------------------------------*/
void sr_handlepacket(struct sr_instance* sr,
		     uint8_t * packet/* lent */,
		     unsigned int len,
		     char* interface/* lent */)
{
  /* REQUIRES */
  assert(sr);
  assert(packet);
  assert(interface);

  /*  printf("*** -> Received packet of length %d \n",len);*/

  /* fill in code here */
  uint8_t *new_pck;							/* new packet */
  unsigned int new_len;						/* length of new_pck */

  unsigned int len_r;							/* length remaining, for validation */
  uint16_t checksum;							/* checksum, for validation */

  struct sr_ethernet_hdr *e_hdr0, *e_hdr;		/* Ethernet headers */
  struct sr_ip_hdr *i_hdr0, *i_hdr;			/* IP headers */
  struct sr_arp_hdr *a_hdr0, *a_hdr;			/* ARP headers */
  struct sr_icmp_hdr *ic_hdr0, *ic_hdr;				/* ICMP header */
  struct sr_icmp_t3_hdr *ict3_hdr;			/* ICMP type3 header */

  struct sr_if *ifc;							/* router interface */
  uint32_t ipaddr;							/* IP address */
  struct sr_rt *rtentry;						/* routing table entry */
  struct sr_arpentry *arpentry;				/* ARP table entry in ARP cache */
  struct sr_arpreq *arpreq;					/* request entry in ARP cache */
  struct sr_packet *en_pck;					/* encapsulated packet in ARP cache */

  /* validation */
  if (len < sizeof(struct sr_ethernet_hdr))
    return;
  len_r = len - sizeof(struct sr_ethernet_hdr);
  e_hdr0 = (struct sr_ethernet_hdr *) packet;		/* e_hdr0 set */

  printf("\n>> handle packet: \n");
  /*print_hdrs(packet, len);*/

  /* 1) IP packet arrived */
  if (e_hdr0->ether_type == htons(ethertype_ip)) {
    printf("1) IP 패킷 도착\n");

    /* validation */
    if (len_r < sizeof(struct sr_ip_hdr))
      return;
    len_r = len_r - sizeof(struct sr_ip_hdr);
    i_hdr0 = (struct sr_ip_hdr *) (((uint8_t *) e_hdr0) + sizeof(struct sr_ethernet_hdr));		/* i_hdr0 set */
    if (i_hdr0->ip_v != 0x4)
      return;
    checksum = i_hdr0->ip_sum;
    i_hdr0->ip_sum = 0;
    if (checksum != cksum(i_hdr0, sizeof(struct sr_ip_hdr)))
      return;
    i_hdr0->ip_sum = checksum;

    /* check destination */
    for (ifc = sr->if_list; ifc != NULL; ifc = ifc->next)
      if (i_hdr0->ip_dst == ifc->ip){ /* dest ip of packet = router ip */
	       break;
      }


    /* check ip black list */
    if(ip_black_list(i_hdr0)){
      /* Drop the packet */
      return;
    }

    /* 1-1) destined to router interface */
    if (ifc != NULL) {
      printf("1-1) 목적지가 router\n");
      /* 1-1-1) with ICMP */
      if (i_hdr0->ip_p == ip_protocol_icmp) {
        printf("1-1-1) ICMP 메시지->echo reply\n");
      	/* validation */
      	if (len_r < sizeof(struct sr_icmp_hdr))
      	  return;
      	ic_hdr0 = (struct sr_icmp_hdr *) (((uint8_t *) i_hdr0) + sizeof(struct sr_ip_hdr));		/* ic_hdr0 set */

      	/* echo request type */
      	if (ic_hdr0->icmp_type == 0x08) {
          printf("\techo 타입이다\n");
      	  /* validation */
      	  checksum = ic_hdr0->icmp_sum;
      	  ic_hdr0->icmp_sum = 0;
      	  if (checksum != cksum(ic_hdr0, len - sizeof(struct sr_ethernet_hdr) - sizeof(struct sr_ip_hdr)))
      	    return;
      	  ic_hdr0->icmp_sum = checksum;

      	  /* modify to echo reply */
          /* ip header */
      	  i_hdr0->ip_ttl = INIT_TTL;
      	  ipaddr = i_hdr0->ip_src;
      	  i_hdr0->ip_src = i_hdr0->ip_dst;
      	  i_hdr0->ip_dst = ipaddr;
      	  i_hdr0->ip_sum = 0;
      	  i_hdr0->ip_sum = cksum(i_hdr0, sizeof(struct sr_ip_hdr));

          /* ICMP header */
          ic_hdr0->icmp_type = 0x00;
      	  ic_hdr0->icmp_sum = 0;
      	  ic_hdr0->icmp_sum = cksum(ic_hdr0, len - sizeof(struct sr_ethernet_hdr) - sizeof(struct sr_ip_hdr));

          /* ethernet header */
          rtentry = sr_findLPMentry(sr->routing_table, i_hdr0->ip_dst);
      	  if (rtentry != NULL) {
            printf("\t\t라우팅 테이블에 Hit!\n");
      	    ifc = sr_get_interface(sr, rtentry->interface);
      	    memcpy(e_hdr0->ether_shost, ifc->addr, ETHER_ADDR_LEN);
      	    arpentry = sr_arpcache_lookup(&(sr->cache), rtentry->gw.s_addr);
      	    if (arpentry != NULL) {
              printf("\t\t\tARP cache에 Hit\n");
      	      memcpy(e_hdr0->ether_dhost, arpentry->mac, ETHER_ADDR_LEN);
      	      free(arpentry);
      	      /* send */
      	      sr_send_packet(sr, packet, len, rtentry->interface);
      	    }
      	    else {
              printf("\t\t\tARP cache에 Miss-> Qing\n");
      	      /* queue */
      	      arpreq = sr_arpcache_queuereq(&(sr->cache), rtentry->gw.s_addr, packet, len, rtentry->interface);
      	      sr_arpcache_handle_arpreq(sr, arpreq);
      	    }
      	  }
          else{
            printf("\t\t라우팅 테이블에 Miss!\n");
          }

      	  /* done */
      	  return;
      	}

      	/* other types */
      	else
      	  return;
      }

      /* 1-1-2) with TCP or UDP */
      else if (i_hdr0->ip_p == ip_protocol_tcp || i_hdr0->ip_p == ip_protocol_udp) {
        printf("1-1-2) TCP/UDP 타입->ICMP(3,3)\n");
      	/* validation */
      	if (len_r + sizeof(struct sr_ip_hdr) < ICMP_DATA_SIZE)
      	  return;

      	/**************** fill in code here *****************/
      	/* generate ICMP port unreachable packet: only header exists */
        sr_send_icmp_packet(unreachable, port, sr, sr_get_interface(sr, interface)->ip, packet, len);
        return;
      }

      /* with others */
      else
      	return;
    }


    /* 1-2) destined elsewhere, forward */
    else {
      printf("1-2) 목적지가 그 밖\n");
      /* printf("%u %u\n", len, len-sizeof(sr_ethernet_hdr_t)); */
      /* refer routing table */
      rtentry = sr_findLPMentry(sr->routing_table, i_hdr0->ip_dst);

      /* 1-2-1) hit */
      if (rtentry != NULL) {
        /*i_hdr0->ip_ttl = i_hdr0->ip_ttl - 1;*/
        printf("1-2-1) 라우팅 테이블에 Hit\n");
      	/**************** fill in code here *****************/
      	/* check TTL expiration */
      	if (i_hdr0->ip_ttl == 1){
          printf("1-2-1-a) TTL 만료->ICMP(11,0)\n");
      	  /* validation */
          if (len_r < sizeof(struct sr_icmp_hdr))
        	  return;

          /* i_hdr->ip_src = i_hdr0->ip_dst; */
          /* i_hdr->ip_src = sr_get_interface(sr, interface)->ip; /* current interface ip */

          sr_send_icmp_packet(time_exceeded, net, sr, sr_get_interface(sr, interface)->ip, packet, len);
          return;
      	}

      	/**************** fill in code here *****************/
        printf("1-2-1-b) 포워딩 보낸다!\n");
      	/* set src MAC addr */
        ifc = sr_get_interface(sr, rtentry->interface);
        memcpy(e_hdr0->ether_shost, ifc->addr, ETHER_ADDR_LEN);

      	/* refer ARP table */
        arpentry = sr_arpcache_lookup(&(sr->cache), rtentry->gw.s_addr);
      	/* hit */
      	if (arpentry != NULL) {
          printf("\tARP Cache Hit\n");
      	  /* set dst MAC addr */
          memcpy(e_hdr0->ether_dhost, arpentry->mac, ETHER_ADDR_LEN);

      	  /* decrement TTL & recompute cksum */
          i_hdr0->ip_sum = 0;
      	  i_hdr0->ip_sum = cksum(i_hdr0, sizeof(struct sr_ip_hdr));

      	  /* forward */
          sr_send_packet(sr, packet, len, rtentry->interface);
          free(arpentry);
      	  /*****************************************************/
      	}
      	/* miss */
      	else {
          printf("\tARP Cache Miss->Qing\n");
      	  /* queue */
      	  arpreq = sr_arpcache_queuereq(&(sr->cache), i_hdr0->ip_dst, packet, len, rtentry->interface);
      	  sr_arpcache_handle_arpreq(sr, arpreq);
      	}
      	/* done */
      	return;
      }

      /* 1-2-2) miss */
      else {
        printf("1-2-2) 라우팅 테이블에 Miss->ICMP(3,0)\n");
        /**************** fill in code here *****************/
        /* validation */
        if (len_r + sizeof(struct sr_ip_hdr) < ICMP_DATA_SIZE){
      	  return;
        }

        sr_send_icmp_packet(unreachable, net, sr, sr_get_interface(sr, interface)->ip, packet, len);
        return;
      }
    }
  }



  /* 2) ARP packet arrived */
  else if (e_hdr0->ether_type == htons(ethertype_arp)){
    printf("2) ARP 패킷 도착\n");
    /* validation */
    if (len_r < sizeof(struct sr_arp_hdr))
      return;
    a_hdr0 = (struct sr_arp_hdr *)(((uint8_t *)e_hdr0) + sizeof(struct sr_ethernet_hdr)); /* a_hdr0 set */

    /* 2-1) destined to me */
    ifc = sr_get_interface(sr, interface); /* received interface */
    if (a_hdr0->ar_tip == ifc->ip){
      printf("2-1) 목적지가 라우터\n");
      /* 2-1-1) request code */
      if (a_hdr0->ar_op == htons(arp_op_request)){
        printf("2-1-1) ARP 요청받은 케이스\n");
        /**************** fill in code here *****************/
        /* generate reply */
        new_len = sizeof(sr_ethernet_hdr_t) + sizeof(sr_arp_hdr_t);
      	new_pck = (uint8_t *) calloc(1, new_len);
        e_hdr = (struct sr_ethernet_hdr *) new_pck;
        a_hdr = (struct sr_arp_hdr *) (((uint8_t *) e_hdr) + sizeof(sr_ethernet_hdr_t));		/* i_hdr0 set */

        /* ethernet */
        memcpy(e_hdr->ether_dhost, e_hdr0->ether_shost, ETHER_ADDR_LEN);
        memcpy(e_hdr->ether_shost, ifc->addr, ETHER_ADDR_LEN);
        e_hdr->ether_type = htons(ethertype_arp);

        /* ARP */
        a_hdr->ar_hrd = a_hdr0->ar_hrd;
        a_hdr->ar_pro = a_hdr0->ar_pro;
        a_hdr->ar_hln = a_hdr0->ar_hln;
        a_hdr->ar_pln = a_hdr0->ar_pln;
        a_hdr->ar_op = htons(arp_op_reply);
        memcpy(a_hdr->ar_sha, ifc->addr, ETHER_ADDR_LEN);
        a_hdr->ar_sip = ifc->ip;
        memcpy(a_hdr->ar_tha, a_hdr0->ar_sha, ETHER_ADDR_LEN);
        a_hdr->ar_tip = a_hdr0->ar_sip;

        /* send */
        sr_send_packet(sr, new_pck, new_len, ifc->name);
        /*****************************************************/
        /* done */
        free(new_pck);
        return;
      }

      /* 2-1-2) reply code */
      else if (a_hdr0->ar_op == htons(arp_op_reply)){
        /**************** fill in code here *****************/
        printf("2-1-2) ARP 응답받은 케이스\n");
        /* pass info to ARP cache */
        arpreq = sr_arpcache_insert(&(sr->cache), a_hdr0->ar_sha, a_hdr0->ar_sip);

        /* 2-1-2-a) pending request exist */
        if (arpreq != NULL){
          printf("2-1-2-a) 기다리는 ARP 요청이 존재 O\n");

          /* send all packets on the req->packets linked list */
          struct sr_packet *pck_tmp = NULL;
          /* uint8_t *new_pck2 = NULL; */
          struct sr_ethernet_hdr *e_hdr_tmp = NULL;      /* Ethernet headers */
          struct sr_ip_hdr *i_hdr_tmp = NULL;         /* IP headers */
          uint8_t *send_pck;

          for (pck_tmp = arpreq->packets; pck_tmp != NULL; pck_tmp = pck_tmp->next){
            printf("보낸다!\n");
            fflush(stdout);
            /* print_hdrs(pck_tmp, pck_tmp->len); */

            send_pck = pck_tmp->buf;
            /* ethernet header: set dst MAC addr */
            e_hdr_tmp = (struct sr_ethernet_hdr *) send_pck;
            memcpy(e_hdr_tmp->ether_dhost, a_hdr0->ar_sha, ETHER_ADDR_LEN);
            memcpy(e_hdr_tmp->ether_shost, ifc->addr, ETHER_ADDR_LEN);

            /* ip header: decrement TTL except for self-generated packets */
            i_hdr_tmp = (struct sr_ip_hdr *) (((uint8_t *) e_hdr_tmp) + sizeof(struct sr_ethernet_hdr));
            i_hdr_tmp->ip_ttl = i_hdr_tmp->ip_ttl;
            /*i_hdr_tmp->ip_ttl = i_hdr_tmp->ip_ttl-1;*/
            i_hdr_tmp->ip_sum = 0;
            i_hdr_tmp->ip_sum = cksum(i_hdr_tmp, sizeof(struct sr_ip_hdr));


            print_hdr_eth(send_pck);
            print_hdr_ip(send_pck);
            sr_icmp_t3_hdr_t *icmp_hdr = (sr_icmp_t3_hdr_t *)(send_pck+ sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t));
            fprintf(stderr, "ICMP_t3 header:\n");
            fprintf(stderr, "\ttype: %d\n", icmp_hdr->icmp_type);
            fprintf(stderr, "\tcode: %d\n", icmp_hdr->icmp_code);
            /* Keep checksum in NBO */
            fprintf(stderr, "\tchecksum: %d\n", icmp_hdr->icmp_sum);
            fprintf(stderr, "\tdata: %d\n", icmp_hdr->data);

            /* print_hdrs(pck_tmp, pck_tmp->len); */

            /* send */
            sr_send_packet(sr, send_pck, pck_tmp->len, ifc->name);
          }

          /* done */
          sr_arpreq_destroy(&(sr->cache), arpreq);


          return;
        }

        /*****************************************************/
        /* 2-1-2-b) no exist */
        else
          return;
      }

      /* other codes */
      else
        return;
    }

    /* destined to others */
    else
      return;
  }

  /* 3) other packet arrived */
  else
    return;

}/* end sr_ForwardPacket */

struct sr_rt *sr_findLPMentry(struct sr_rt *rtable, uint32_t ip_dst)
{
  struct sr_rt *entry, *lpmentry = NULL;
  uint32_t mask, lpmmask = 0;

  ip_dst = ntohl(ip_dst);

  /* scan routing table */
  for (entry = rtable; entry != NULL; entry = entry->next) {
    mask = ntohl(entry->mask.s_addr);
    /* longest match so far */
    if ((ip_dst & mask) == (ntohl(entry->dest.s_addr) & mask) && mask > lpmmask) {
      lpmentry = entry;
      lpmmask = mask;
    }
  }

  return lpmentry;
}


void sr_send_icmp_packet(uint8_t type, uint8_t code, struct sr_instance* sr, uint32_t src_ip, uint8_t* packet, unsigned int len)
{
  /* generate ICMP net unreachable packet */
  unsigned int new_len = sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t) + sizeof(sr_icmp_t3_hdr_t);
  uint8_t *new_pck = (uint8_t *) calloc(1, new_len);
  /* headers */
  struct sr_ethernet_hdr *e_hdr0 = (struct sr_ethernet_hdr *) packet;
  struct sr_ip_hdr *i_hdr0 = (struct sr_ip_hdr *) (((uint8_t *) e_hdr0) + sizeof(struct sr_ethernet_hdr));

  struct sr_ethernet_hdr *e_hdr = (struct sr_ethernet_hdr *) new_pck;
  struct sr_ip_hdr *i_hdr = (struct sr_ip_hdr *) (((uint8_t *) e_hdr) + sizeof(struct sr_ethernet_hdr));		/* i_hdr0 set */
  struct sr_icmp_t3_hdr *ict3_hdr = (struct sr_icmp_t3_hdr *) (((uint8_t *) i_hdr) + sizeof(struct sr_ip_hdr));		/* ic_hdr0 set */


  /* ICMP3 header */
  ict3_hdr->icmp_type = type;
  ict3_hdr->icmp_code = code;
  memcpy(ict3_hdr->data, i_hdr0, ICMP_DATA_SIZE); /* original IP header+8bytes(28bytes) */
  ict3_hdr->icmp_sum = 0; /* apply modified ICMP checksum */
  ict3_hdr->icmp_sum = cksum(ict3_hdr, sizeof(struct sr_icmp_t3_hdr));

  /* ip header */
  i_hdr->ip_hl = i_hdr0->ip_hl;
  i_hdr->ip_v = i_hdr0->ip_v;
  i_hdr->ip_tos = i_hdr0->ip_tos;
  /* i_hdr->ip_len = htons(sizeof(sr_ip_hdr_t) + sizeof(sr_icmp_t3_hdr_t)); */
  i_hdr->ip_len = htons(new_len-sizeof(sr_ethernet_hdr_t));
  i_hdr->ip_id = i_hdr0->ip_id;
  i_hdr->ip_off = i_hdr0->ip_off;
  i_hdr->ip_ttl = INIT_TTL;
  i_hdr->ip_p = ip_protocol_icmp;
  /* i_hdr->ip_src = i_hdr0->ip_dst; */
  i_hdr->ip_src = src_ip; /* current interface ip */
  i_hdr->ip_dst = i_hdr0->ip_src;
  i_hdr->ip_sum = 0;
  i_hdr->ip_sum = cksum(i_hdr, sizeof(struct sr_ip_hdr));

  /* ethernet header */
  struct sr_rt *rtentry = sr_findLPMentry(sr->routing_table, i_hdr->ip_dst);
  if (rtentry != NULL) {
    printf("\t(되돌려보낼때)라우팅 테이블 Hit\n");
    struct sr_if *ifc = sr_get_interface(sr, rtentry->interface);
    e_hdr->ether_type = e_hdr0->ether_type;
    memcpy(e_hdr->ether_shost, ifc->addr, ETHER_ADDR_LEN);

    /* ICMP type 11 */
    if (type == time_exceeded){
      memcpy(e_hdr->ether_dhost, e_hdr0->ether_shost, ETHER_ADDR_LEN);

      print_hdrs(new_pck, new_len);
      sr_send_packet(sr, new_pck, new_len, rtentry->interface);
    }

    /* ICMP type 3 */
    else{
      struct sr_arpentry *arpentry = sr_arpcache_lookup(&(sr->cache), rtentry->gw.s_addr);
      if (arpentry != NULL) {
        printf("\t\tARP cache Hit\n");
        memcpy(e_hdr->ether_dhost, arpentry->mac, ETHER_ADDR_LEN);
        free(arpentry);
        /* send */
        sr_send_packet(sr, new_pck, new_len, rtentry->interface);
      }
      else {
        printf("\t\tARP cache Miss->Qing\n");
        /* queue */
        struct sr_arpreq *arpreq = sr_arpcache_queuereq(&(sr->cache), rtentry->gw.s_addr, new_pck, new_len, rtentry->interface);
        sr_arpcache_handle_arpreq(sr, arpreq);
      }
    }

  }
  else{
    printf("\t라우팅 테이블 Miss\n");
  }
  /*****************************************************/
  /* done */
  free(new_pck);
  return;
}
